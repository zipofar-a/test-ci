# Основные принципы

## Gitlab позволяет бесплатно:

* Хранить docker образы
* Осуществлять сборку образов внутри докера
* Запускать docker образы (в т.ч. с помощью docker-compose) для прогона тестов, линтеров и т.д.

## Что примерно делает для нас Gitlab

* Запускает контейнер DIND (Docker In Docker), внутри которого мы можем использовать обычный docker
* Клонирует в контейнер наш репозиторий
* Позволяет вставить секретные переменные окружения через интерфейс, чтобы в дальнейшем мы могли использовать их в контейнере  DIND или в файл `.gitlab-ci.yml`
* Ищет файл `.gitlab-ci.yml` в корне нашего репозитория, для применения его в качестве конфиг файла при запуске своего специального `runner-а`

## Как протекает процесс CI

### Создание docker образа

Для начала нам надо создать docker образ с нашим проектом, затем сохранить его (запушить) в репозитории Gitlab. Для этого у нас есть примерно такой код в `.gitlab-ci.yml`


```
build_web:
  image: docker:18
  services:
    - name: docker:18-dind
      entrypoint: ["env", "-u", "DOCKER_HOST"]
      command: ["dockerd-entrypoint.sh"]
  before_script:
    - export GITLAB_ACCESS_TOKEN=$GL_TOKEN
    - apk add --no-cache make bash git
    - apk add --no-cache curl jq python py-pip
    - pip install --no-cache-dir docker-compose==1.22.0
    - docker-compose -v
  stage: build
  script:
    - make pull_image VERSION=latest || true
    - make build_image push_image
```

Пройдемся по основным моментам.


#### 1 Get Gitlab Access Token
`export GITLAB_ACCESS_TOKEN=$GL_TOKEN` - экспортируем gitlab access token в качестве переменной окружения в контейнер, который для нас создаст gitlab. `$GL_TOKEN` - эта переменная определена в web интерфейсе gitlab.

#### 2 Pull image
`make pull_image VERSION=latest || true` - скачиваем последнюю версию образа из нашего репозитория на gitlab. Если ее нет, то вернем `true`, чтобы продолжить выполнение скриптов.

Чтобы скачать образ, используем команду в makefile.
```
# Адрес нашего репозитория, который предоставил нам gitlab
WEB_REPOSITORY = "registry.gitlab.com/zipofar/test-ci"

# Если данная переменная существует как переменная окружения, либо передана в
# качестве аргумента в makefile, то используется это значение. Иначе используется
# hash последнего коммита
VERSION ?= $$(git rev-parse HEAD)

# Для того, чтобы пушить и пулить образы, необходимо сначала залогиниться в gitlab
ecr_login:
	docker login registry.gitlab.com -u  some@mail.com -p ${GITLAB_ACCESS_TOKEN}

pull_image: ecr_login
	docker pull ${WEB_REPOSITORY}:${VERSION}
```

#### 3 Build, Push image
`make build_image push_image` - здесь и происходит самое интересное. Создание образа и отправка его в репозиторий gitlab.

Makefile
```
# cache-from - тут мы берем предыдущий last образ (для этого мы и скачали образ
# на предыдущем этапе) и говорим докеру, чтобы он его использовать в качестве кэша.
# Чтобы избежать полной пересборки образа и экономим тем самым время при создании нового образа.
#
# -t - это тегирование образа или присвоение каждому образу уникального имени.
# Можно задавать несколько тэгов одному контейнеру.
# Тут мы задаем уникальное имя и так же присваиваем ему имя latest, т.к. теперь он
# будет являться последним актуальным образом.

build_image:
	docker build --cache-from ${WEB_REPOSITORY}:latest -t ${WEB_REPOSITORY}:${VERSION} -t ${WEB_REPOSITORY}:latest .


# В нашем текущем gitlab контейнере у нас существует только один образ с двумя тэгами.
# Но в репозитории нам надо иметь только один образ с тэгом latest. Поэтому пушим по
# сути один и тот же образ. но с разными тэгами.

push_image: ecr_login
	docker push ${WEB_REPOSITORY}:${VERSION}
	docker push ${WEB_REPOSITORY}:latest
```
